
echo "DROPBEAR:"
CONF_DIR="/etc/dropbear"
SSH_KEY_DSS="${CONF_DIR}/dropbear_dss_host_key"
SSH_KEY_RSA="${CONF_DIR}/dropbear_rsa_host_key"
SSH_KEY_ECDSA="${CONF_DIR}/dropbear_ecdsa_host_key"
SSH_KEY_ED25519="${CONF_DIR}/dropbear_ed25519_host_key"

# Check if conf dir exists
if [ ! -d ${CONF_DIR} ]; then
    mkdir -p ${CONF_DIR}
fi

chown root:root ${CONF_DIR}
chmod 755 ${CONF_DIR}

# Check if keys exists
if [ ! -f ${SSH_KEY_RSA} ]; then
    dropbearkey  -t rsa -f ${SSH_KEY_RSA} -s 4096
    chown root:root        ${SSH_KEY_RSA}
    chmod 600              ${SSH_KEY_RSA}
fi & 


## OpenSSH 7.0 and greater similarly disables the ssh-dss (DSA) public key algorithm. It too is weak and we recommend against its use. 
rm ${SSH_KEY_DSS} 2>/dev/null || true &
## Check if keys exists
#if [ ! -f ${SSH_KEY_DSS} ]; then
#    dropbearkey  -t dss -f   ${SSH_KEY_DSS};    chown root:root          ${SSH_KEY_DSS};    chmod 600                ${SSH_KEY_DSS} 
#fi &

if [ ! -f ${SSH_KEY_ED25519} ]; then
    dropbearkey  -t ed25519 -f ${SSH_KEY_ED25519}
    chown root:root          ${SSH_KEY_ED25519}
    chmod 600                ${SSH_KEY_ED25519}

fi &

if [ ! -f ${SSH_KEY_ECDSA} ]; then
    dropbearkey  -t ecdsa -f ${SSH_KEY_ECDSA} -s 521
    chown root:root          ${SSH_KEY_ECDSA}
    chmod 600                ${SSH_KEY_ECDSA}

fi &

test -f /usr/libexec/sftp-server || (mkdir -p /usr/libexec/ && ln -s /usr/lib/ssh/sftp-server /usr/libexec/sftp-server)

########### WEBROOT / DROPBEAR / PERMISSION HUSSLE 
test -d /var/www/.ssh || ( mkdir /var/www/.ssh ;chown www-data:www-data /var/www/.ssh;touch /var/www/.ssh/authorized_keys;chmod 0600 /var/www/.ssh/authorized_keys /var/www/.ssh )
test -f /var/www/.ssh/authorized_keys && chown www-data:www-data /var/www/.ssh/authorized_keys
test -f /var/www/.ssh/authorized_keys && ( chmod 600 /var/www/.ssh/authorized_keys ;chmod ugo-w /var/www/.ssh/authorized_keys) 
test -d /var/www/.ssh && (chown www-data:www-data /var/www/.ssh ;chmod u+x /var/www/.ssh) &
test -d /root/.ssh || ( mkdir /root/.ssh;touch /root/.ssh/authorized_keys ; chmod 0600 /root/.ssh /root/.ssh/authorized_keys ) &


while true ;do dropbear -R -j -k -s -g -E -F ;sleep 3 ; done

