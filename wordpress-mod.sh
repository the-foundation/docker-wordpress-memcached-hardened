#!/bin/bash
DEBUG_PHP="$1"
echo adminer

test -f /var/www/.toolkit || mkdir -p /var/www/.toolkit
RELEASES=$(curl -s https://github.com/vrana/adminer/tags|grep /releases/tag |grep href|cut -d'"' -f4|grep /releases|head -n1)
RELEASEDOWNLOADS=$(curl -s https://github.com/$RELEASES|grep  expanded_assets|sed 's/.\+src="//g'|cut -d '"' -f1)
TARGFILE=$(curl -s $RELEASEDOWNLOADS|grep /releases/|cut -d'"' -f2|grep [0-9]/adminer.*[0-9].php|head -n1 )
test -f /var/www/.toolkit/sql.php || wget -q -O /var/www/.toolkit/sql.php -c "https://github.com/$TARGFILE" &

#sqlurl=$(curl -skL $(curl -kL https://github.com/vrana/adminer/releases/latest|grep github.com/vrana/adminer/releases/|sed 's/.\+http/http/g;s/".\+//g'|grep -v quot|grep ^http|head -n1 )|grep php|grep download|grep [0-9]/adminer.*[0-9].php|grep href|cut -d'"' -f2|sed 's/^/https:\/\/github.com/g')
#test -f /var/www/.toolkit/sql.php || wget -O /var/www/.toolkit/sql.php -c "$sqlurl" &

echo memcachedinstall
ls -1  /usr/local/lib/php/extensions ;

ls -1  /usr/local/lib/php/extensions/ |while read destfold;do echo $destfold; ln -s /usr/lib/php7/modules/memcached.so  /usr/local/lib/php/extensions/$destfold/memcached.so || true ;done
ls -1  /usr/local/lib/php/extensions/ |while read destfold;do  ln -s /usr/lib/php7/modules/memcached.so  /usr/local/lib/php/extensions/$destfold/memcached.so.so || true ;done

test -d /etc/dockermail || mkdir /etc/dockermail

if [ "$MAIL_DRIVER" = "msmtp" ] ; then
    if [ ! -f /etc/dockermail/php-mail.conf ]; then
        echo "creating phpmail ssmtp entry"
        echo "c2VuZG1haWxfcGF0aCA9IC91c3IvYmluL21zbXRwIC10Cg=="|base64 -d > /etc/dockermail/php-mail.conf
    fi
    ### AUTO-UPGRADE SSMTP(OUTDATED) CONTAINERS
    grep ssmtp /etc/dockermail/php-mail.conf -q && ( echo "c2VuZG1haWxfcGF0aCA9IC91c3IvYmluL21zbXRwIC10Cg=="|base64 -d > /etc/dockermail/php-mail.conf  )

    if [ -z "${MAIL_HOST}" ]; then
        echo "MAIL_HOST NOT SET in .env, not setting up MSMTP"
    else
        if [ -z "${MAIL_FROM}" ]; then
            echo "MAIL_FROM NOT SET in .env, not setting up MSMTP"
        else
        (   echo "YWxpYXNlcyAgICAgICAgICAgICAgIC9ldGMvYWxpYXNlcy5tc210cAoKIyBVc2UgVExTIG9uIHBvcnQgNTg3CnBvcnQgNTg3CnRscyBvbgp0bHNfc3RhcnR0bHMgb24KdGxzX3RydXN0X2ZpbGUgL2V0Yy9zc2wvY2VydHMvY2EtY2VydGlmaWNhdGVzLmNydAojIFRoZSBTTVRQIHNlcnZlciBvZiB5b3VyIElTUAo="|base64 -d ;
            echo "host ${MAIL_HOST}";
            echo "domain ${APP_URL}";
            echo "maildomain ${APP_URL}";
            echo "user ${MAIL_USERNAME}";
            echo "password ${MAIL_PASSWORD}";
            echo "auto_from off";
            echo "auth on";
            echo "logfile -";
            if [ -z "${MAIL_FROM}" ];then
              ## no mail from , but a mail adress as username
              echo ${MAIL_USERNAME} |grep -q "@" &&  echo  "from ${MAIL_USERNAME}";
              ## no mail from , no mail adress as username , we take no-reply@${APP_URL}
              echo ${MAIL_USERNAME} |grep -q "@" ||  echo  "from no-reply@${APP_URL}";
            else
              echo "from ${MAIL_FROM}"
            fi
             ) > /etc/dockermail/msmtprc
        fi
        if [ -z "${MAIL_ADMINISTRATOR}" ];
            then echo "::MAIL_ADMINISTRATOR not set FIX THIS !(msmtp)"
            else for user in www-data mailer-daemon postmaster nobody hostmaster usenet news webmaster www ftp abuse noc security root default;	do echo "$user: "${MAIL_ADMINISTRATOR} >> /etc/aliases.msmtp;done
        fi
        ## IF the special mail username is used, we send directly without auth and tls
        if [ "$MAIL_USERNAME" = "InternalNoTLSNoAuth" ] ;then echo "using direct smtp port 25 with no auth and no tls" ;sed 's/tls_starttls.\+/tls_starttls off/g;s/^tls on/tls off/g;s/^auth on/auth off/g;s/^port .\+/port 25/g'  /etc/dockermail/msmtprc -i ;fi

    fi

    if [ -f /etc/dockermail/msmtprc ]; then
        ln -sf /etc/dockermail/msmtprc /etc/msmtprc
    fi
#chown www-data:www-data /etc/msmtprc
ln -sf /etc/dockermail/php-mail.conf /usr/local/etc/php/conf.d/mail.ini

fi

sed 's/.usr.bin.msmtp -t/\/usr\/sbin\/sendmail/g' -i /etc/dockermail/php-mail.conf

sed 's/^php_flag.display_errors.\+//g' -i /usr/local/etc/php-fpm.d/www.conf 
(
[[ "$DEBUG_PHP" = "true" ]] && echo 'php_flag[display_errors] = on';
[[ "$DEBUG_PHP" = "true" ]] || echo 'php_flag[display_errors] = off'; 
echo 
 )>> /usr/local/etc/php-fpm.d/www.conf 
echo sleeping 5s
sleep 5
