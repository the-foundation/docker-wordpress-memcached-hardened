# Hardened Wordpress Memcached Docker Setup

## Installation

* create a proper .env with **(!) secure (!)** passwords and all variables like smtp and SSH_PORT filled
* **hint:** use `TOKEN_GUEST_PASS=NoPassword` ( default if not set) to disable htpasswd protection from user login
* use `MAIL_USERNAME=InternalNoTLSNoAuth` to use port 25 unencrypted for smtp ( internal use only , properly configured servers should require STARTTLS)
* navigate to settings->WP-FFPC and verify that the memcached backend is up and running  or change the address to `memcached:11211`
* under settings->WP-FFPC press save once to activate memcached
* Decide what setup you need :
* -> either a multi-setup with a www.APP_URL container redirecting to APP_URL `ln -s docker-compose-multi.yml docker-compose.yml`
* -> OR a single-setup with one webserver handling www.APP_URL and APP_URL `ln -s docker-compose-single.yml docker-compose.yml` ( e.g. if you migrate and the setup was on www.anydomain.lan )

## IMPORTANT
 when building fails during make, you might try to pass
 `--security-opt=seccomp=seccomp-default.json`
 to docker, or upgrade your docker daemon to 20.10.x+

## Debugging

* use `docker ps -a` to see the containers or `docker ps -a --format '{{.Names}}'`
* and then e.g. `docker logs --since 20m mycontainer.name` to see them
* or use `docker logs -f` to follow ( e.g. `docker logs mycontainer --until 10m --from 60m` )

OR

* run `docker-compose pull;docker-compose up --build ` to see the logs whilst starting containers

## Locked stuff ( hardening )

* no external /wp-cron since internal cron triggers it
* no xmlprpc
* only partial json-api
* password protected `/wp-admin` and `/wp-login.php` ( `TOKEN_USER` and `TOKEN_PASS`)
* php not executed in uploads folder
* mime types fixed
* no `.FOLDERNAME` folders
* no `.git` or `.svn`

## Toolkit

there is a htaccess secured folder found under /.toolkit/ ,
providing adminer ( `/.toolkit/sql.php` ) and memcached ( `/.toolkit/cache/` )
admin interfaces by default , protected by the same .htaccess credentials used for
/wp-admin


## example .env:

( leave `MAIL_DRIVER` and `WORDPRESS_DB_HOST` **untouched** except you know what you are doing)

```
APP_URL=yourproject.com
WORDPRESS_ADMIN_USER=NameOfAdminUser
WORDPRESS_ADMIN_PASS=PasswordOfAdminUser
WORDPRESS_ADMIN_MAIL=default-mail-not-set@using-fallback-default.slmail.me

MAIL_ADMINISTRATOR=default-mail-not-set@using-fallback-default.slmail.me
TOKEN_USER=htaccess.user
TOKEN_PASS=htaccess.passowrd
TOKEN_GUEST_PASS=normal.user.login.pass
MAIL_HOST=your.smtp.server.lan
MAIL_USERNAME=smtp.username
MAIL_PASSWORD=smtp.password
MAIL_FROM=default-mail-not-set@using-fallback-default.slmail.me
MAIL_DRIVER=smtp
WORDPRESS_DB_HOST=database
WORDPRESS_DB_USER=mysql-user
WORDPRESS_DB_PASSWORD=mysql-password
WORDPRESS_DB_NAME=wordpress
MYSQL_ROOT_PASSWORD=secure.mysql.root.pass
SSH_PORT=00000
COMPOSE_PROJECT_NAME=yourproject.com
STORAGE_ROOT=/storage_basepath
```
---


<h3>A project of the foundation</h3>
<a href="https://the-foundation.gitlab.io/"><div><img src="https://hcxi2.2ix.ch/gitlab/the-foundation/docker-wordpress-memcached-hardened/README.md/logo.jpg" width="480" height="270"/></div></a>
