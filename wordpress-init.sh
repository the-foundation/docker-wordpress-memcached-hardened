#/bin/bash

echo "SOCAT PRECACHE BYPASS:"

which socat  || echo socat missing

which socat 2>/dev/null && (
while (true);do socat TCP-LISTEN:80,fork TCP:${APP_URL}_webserver:80 ;sleep 3;done & 

#FILENAME=socert
#echo "[+] Generating public and private keys pair (.key)...";openssl genrsa -out $FILENAME.key 1024 ;  echo "[+] Generating a self-signed x509 CA's certificate (.crt)...";(sleep 2 ; echo ;echo;echo;echo;echo;echo)openssl req -new -key $FILENAME.key -x509 -sha256 -days 3600 -out $FILENAME.crt; echo "[+] Generating the PEM file out of the key and certificate files...";cat $FILENAME.key $FILENAME.crt > $FILENAME.pem echo -e "\n[>] Certificate's dump:"
#openssl x509 -in $FILENAME.pem -text -noout

#echo -e "\n[>] Generated files:";echo -e "\tPKI keys (public/private):\t$FILENAME.key";echo -e "\tCA Certficate:\t\t$FILENAME.crt";echo -e "\tResulting PEM:\t\t$FILENAME.pem"
#while (true);do socat -v -v openssl-listen:443,reuseaddr,fork,cert=$FILENAME.pem,cafile=$FILENAME.crt,verify=0 TCP:${APP_URL}_webserver:80;sleep 3 ;done & ) &


#while (true);do socat -v -v TCP-LISTEN:443,reuseaddr,fork TCP:$(curl -kLv http://whatismyip.akamai.com/):443 ;sleep 3 ;done & ) &
## make the external ip visible from 127.0.0.1 to provide proper ssl from inside
while (true);do timeout 7200 socat  TCP-LISTEN:443,reuseaddr,fork TCP:$(curl -s http://whatismyip.akamai.com/):443 ;sleep 3 ;done & ) &


#########
echo -n HTPASS:
test -f /var/htpass/.htpasswd || ( echo "HTPASS-GEN"
failed=no

if [ -z ${TOKEN_USER} ] ;then failed=yes;fi
if [ -z ${TOKEN_PASS} ] ;then failed=yes;fi

echo "$failed" |grep -q yes && ( echo "TOKEN_PASS OR TOKEN_USER MISSING , generating RANDOM HTPASSWD ";TOKEN_USER="$(cat /dev/urandom |tr -cd '[:alnum:]_\-'  |head -c 10)" ;TOKEN_PASS="$(cat /dev/urandom |tr -cd '[:alnum:]_\-,.'  |head -c 24)"  ; htpasswd -cbBC 10 /var/htpass/.htpasswd "${TOKEN_USER}" "${TOKEN_PASS}" )
echo "$failed" |grep -q yes || (echo "GENERATING HTPASSWD FROM ENV"; htpasswd -cbBC 10 /var/htpass/.htpasswd "${TOKEN_USER}" "${TOKEN_PASS}" )

)

user_htpass=no

if [ -z ${TOKEN_GUEST_PASS} ] ;then user_htpass=no;else user_htpass=yes;fi

echo -n "use htpass for user:"
echo $user_htpass |grep ^yes$ && {

echo "HTPASS-GUEST-GEN ( username : users )"

test -f /var/htpass/.htpasswd || (echo "GENERATING HTPASSWD FROM ENV"; htpasswd -bBC 10 /var/htpass/.htpasswd.user "users" "${TOKEN_GUEST_PASS}" )

test -f /var/htpass/.htpasswd && ( cat /var/htpass/.htpasswd > /var/htpass/.htpasswd.user ;(echo "GENERATING HTPASSWD FROM ENV"; htpasswd -bBC 10 /var/htpass/.htpasswd.user "users" "${TOKEN_GUEST_PASS}" ) )


    }



( echo "setting up mail for wp-cli";

test -d /etc/dockermail || mkdir /etc/dockermail

if [ "$MAIL_DRIVER" = "msmtp" ] ; then
    if [ ! -f /etc/dockermail/php-mail.conf ]; then
        echo "creating phpmail ssmtp entry"
        echo "c2VuZG1haWxfcGF0aCA9IC91c3IvYmluL21zbXRwIC10Cg=="|base64 -d > /etc/dockermail/php-mail.conf
    fi
    ### AUTO-UPGRADE SSMTP(OUTDATED) CONTAINERS
    grep ssmtp /etc/dockermail/php-mail.conf -q && ( echo "c2VuZG1haWxfcGF0aCA9IC91c3IvYmluL21zbXRwIC10Cg=="|base64 -d > /etc/dockermail/php-mail.conf  )

    if [ -z "${MAIL_HOST}" ]; then
        echo "MAIL_HOST NOT SET in .env, not setting up MSMTP"
    else
        if [ -z "${MAIL_FROM}" ]; then
            echo "MAIL_FROM NOT SET in .env, not setting up MSMTP"
        else
        (   echo "YWxpYXNlcyAgICAgICAgICAgICAgIC9ldGMvYWxpYXNlcy5tc210cAoKIyBVc2UgVExTIG9uIHBvcnQgNTg3CnBvcnQgNTg3CnRscyBvbgp0bHNfc3RhcnR0bHMgb24KdGxzX3RydXN0X2ZpbGUgL2V0Yy9zc2wvY2VydHMvY2EtY2VydGlmaWNhdGVzLmNydAojIFRoZSBTTVRQIHNlcnZlciBvZiB5b3VyIElTUAo="|base64 -d ;
            echo "host ${MAIL_HOST}";
            echo "domain ${APP_URL}";
            echo "user ${MAIL_USERNAME}";
            echo "password ${MAIL_PASSWORD}";
            echo "auto_from off";
            echo "auth on";
            echo "logfile -"
            if [ -z "${MAIL_FROM}" ];
            	then echo "from ${MAIL_USERNAME}";
            	else echo "from ${MAIL_FROM}"
            fi
             ) > /etc/dockermail/msmtprc
        fi
        if [ -z "${MAIL_ADMINISTRATOR}" ];
            then echo "::MAIL_ADMINISTRATOR not set FIX THIS !(msmtp)"
            else for user in www-data mailer-daemon postmaster nobody hostmaster usenet news webmaster www ftp abuse noc security root default;	do echo "$user: "${MAIL_ADMINISTRATOR} >> /etc/aliases.msmtp;done
        fi
        ## IF the special mail username is used, we send directly without auth and tls
        if [ "$MAIL_USERNAME" = "InternalNoTLSNoAuth" ] ;then echo "using direct smtp port 25 with no auth and no tls" ;sed 's/tls_starttls.\+/tls_starttls off/g;s/^tls on/tls off/g;s/^auth on/auth off/g;s/^port .\+/port 25/g'  /etc/dockermail/msmtprc -i ;fi

    fi

    if [ -f /etc/dockermail/msmtprc ]; then
        ln -sf /etc/dockermail/msmtprc /etc/msmtprc
    fi
#chown www-data:www-data /etc/msmtprc
ln -sf /etc/dockermail/php-mail.conf /usr/local/etc/php/conf.d/mail.ini
fi

#sed 's/.usr.bin.msmtp -t/\/usr\/sbin\/sendmail/g' -i /etc/dockermail/php-mail.conf
#ln -sf /usr/bin/msmtp /usr/sbin/sendmail

) &


cd /var/www/html

echo inital insmonia 5s;sleep 15;

id -u;

test -d /var/www/.toolkit || mkdir -p /var/www/.toolkit

mkdir -p /var/www/.wp-cli/cache/

chown  www-data /var/www/html

chown -R www-data /var/www/.wp-cli/cache/ /var/www/.toolkit;

su -s /bin/sh -c 'echo -n i "should be www-data@33|i am www-data@"  ; id -u; cd /var/www/html;/bin/bash /wordpress-init-wwwdata.sh ' www-data

#while (true); do
#  su -s /bin/bash -c "cd /var/www/html; wp plugin list 2>/dev/shm/wp_cron_pluginlist.error.log |grep ^wordpress-seo |grep -v inactive|grep active -q && { wp yoast index --no-color --debug &>/dev/shm/wp_cron_yoast.log ; } ; " www-data
#  sleep 3600 ;
#done &

while (true); do
  /bin/bash /bin/wp-cron.sh
  sleep 120 ;
done &
sleep 10

while (true); do 
  su -s /bin/bash -c "cd /var/www/html; wp plugin list 2>/dev/shm/wp_cron_pluginlist.error.log |grep ^wordpress-seo |grep -v inactive|grep active -q && { wp yoast index --no-color --debug &>/dev/shm/wp_cron_yoast.log; ( wp plugin list 2>&1 |grep -q webp-express || wp plugin install webp-express ) &>>/dev/shm/wepb-express.log ; ( cd /var/www/html/wp-content/;time wp webp-express convert --converter=cwebp --near-lossless=85  uploads &>/dev/shm/webp.convert.log )   ; } ; " www-data
  sleep 3590 ; 
done &



