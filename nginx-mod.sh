#!/bin/bash

if [ "$TOKEN_GUEST_PASS" = "NoPassword" ] ;then 
    #sed 's/auth_basic_user_file \/var\/htpass\/.htpasswd.user/#auth_basic_user_file \/var\/htpass\/.htpasswd.user/g' /etc/nginx/conf.d/default.conf -i
   sed '$!N;s/auth_basic .\+\;\nauth_basic_user_file \/var\/htpass\/\.htpasswd\.user\;/#auth_basic "Login protected with 2FA"\;\n#auth_basic_user_file \/var\/htpass\/\.htpasswd\.user\;/;P;D' /etc/nginx/conf.d/default.conf  -i

else
    #sed 's/#auth_basic_user_file \/var\/htpass\/.htpasswd.user/auth_basic_user_file \/var\/htpass\/.htpasswd.user/g' /etc/nginx/conf.d/default.conf -i
    sed '$!N;s/#auth_basic .\+\;\n#auth_basic_user_file \/var\/htpass\/\.htpasswd\.user\;/auth_basic "Login protected with 2FA"\;\nauth_basic_user_file \/var\/htpass\/\.htpasswd\.user\;/;P;D' /etc/nginx/conf.d/default.conf  -i
fi

if [ "$PASSWORDFORALL" = "true" ] ; then
sed 's/#ALLPASSWD \(.\+\)/        \1 #ALLPASSWDENABLED/g' /etc/nginx/conf.d/default.conf  -i
grep WD /etc/nginx/conf.d/default.conf

else
sed 's/\(.\+\)#ALLPASSWDENABLED$/#ALLPASSWD \1/g' /etc/nginx/conf.d/default.conf  -i
fi
