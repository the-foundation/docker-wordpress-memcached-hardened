#!/bin/bash 

id -u

test -f /var/www/html/.existingproject && (
echo "existing project, simulating upgrades"
wp core check-update --path=/var/www/html ;
wp plugin update --dry-run --all --path=/var/www/html ; 
wp theme update --dry-run --all --path=/var/www/html ; 
)


test -e /var/www/html/wp-content/plugins/wp-ffpc && rm -rf /var/www/html/wp-content/plugins/wp-ffpc &

test -e /var/www/html/.existingproject || (
    echo installing;
    echo -n "core:"
    wp core install --path=/var/www/html --url="${APP_URL}" --title="New Wordpress Site" --admin_user=${WORDPRESS_ADMIN_USER} --admin_password=${WORDPRESS_ADMIN_PASS} --admin_email=${WORDPRESS_ADMIN_MAIL} ; 
    
    echo trying update; sleep 1; 
    
    wp core update --path=/var/www/html ;wp core update-db --path=/var/www/html; 
    wp plugin update --all --path=/var/www/html ; 
    wp theme update --all --path=/var/www/html; 
    
    echo "favicon"
    
    test -f /var/www/html/favicon.ico || wget -O/var/www/html/favicon.ico  https://wordpress.org/favicon.ico ;
    wp plugin install w3-total-cache
    wp plugin activate w3-total-cache
    test -e  /var/www/html/wp-content/w3tc-config/ || (
         for m in $(seq 1 $(cat /w3tc-template.sql |wc -l ));do head -n $m /w3tc-template.sql |tail -n1 |  mysql -u ${WORDPRESS_DB_USER} -p${WORDPRESS_DB_PASSWORD} -h ${WORDPRESS_DB_HOST} ${WORDPRESS_DB_NAME} ;done
        (cd /;
        echo "H4sIAAAAAAAAA+xc+4/jNpLOz/tXNBoBNjNZuW33aya5XJCdDW4Hl2TmMnPA4uKcQEm0zW5J1JCU3Z5k//erIiVbj6Ikbw45HLA9gHua38dXVbFYfHnH1NV+v7/amiy92hdBLHPDc3O1vzYx/rEWm6tPfuPPHH7ub2/t7/nNwv5eLJfub/fzyeLm5nq5vJ7fIr64v5/ffnJx+1srnvJTasPUxcUnIIQgYYb5eGP4/9Of3QT9izzhTzNk/GN1oILvbm4m6X8+h/Tl4u5u/snF/H+3q/TPP/U/qv+MacPVrNgW/1gdw/q/v72+XbTH/3Jxv7z5p/5/j59/+RrUesGfhPny4ut//eUPF/BzueNKC5lffnFxuZxdz5aXf3LpcVpaS8i41mzDo1LPEvjcAHHNUs39NJ6zKOXJOFHneqb4pqp9oGIkskKEj/wwkal5rLiZQDayEHHIVKsJSRSzeMtnbkyUihloYyhBVKlkSa9rNZ2STwsLi1JtuIdxEptRZR/ciBxzXmY8s0lJt7FrkfLZJgbO9d18ToGpjB9F7mthKtbciAwrWbzo5j/WOivQXjT6DbqlJybb65CVRiZCxyi7g6fiUw7NFZoj8H6yFEs7wl8sFsvF4tIiP3vLKKGQnNlu9PTZ6AXTei9VMsyKRM7UISyUNDKWKd1hxaGDo2JxLLKDi+X9bA7/Fl/cXd+/pPvnskNesT6EJtVhzJURaxEzw/VguwY66hhJJBDtatyBaBCyNF4cBhkISMA8onYsHaCxJPSXBToLwUKNEw1lI4o/8NjggAQXnpuOBL95+zb88dv/+M9v372v+miT//Lm9Q//Fr768c0P7dQf3rx/9c2rv377lz8309/99c2P71//8LpVxN++/+7Ht6+OhXt0U7VNPgo0u588hFRuNr7xXVH0h7Tds02iVdhs0L4I1aaVEkKSBqeGLqqdHp/ShxpeKtGudJCNxtQR/3+vVvq5yMG4zWoVNdtgkYSn3HAKKQuYZUlE8SJlMQmBY/dkYmhBFKC3ck+m826D4U90WejmgfD5V/jxxwPXf+yQQFFhzNI4XMsyT0Il97pfUhP7bLV61pGrjJWUBgz/qfL9HdtP2ccDNsQzoR5hs1Ucepi2xvgRBecVgxWEItu0Ta/HiFj8uFHYZg9xI+Um5RkrdPXfEP8fcqa7rp3KAmbayHVuhrBIS5gCvSJ6gvk94W27ZDvBgmgTaHNIeQByPk6aTgEQ4QFcSC1wfv+qh/F8JxQLtIr7WMpgJgehQihBgR8PgVQCGsxSD+wp1fWHRsgsubRYHyDZkAhG30pfl2laqnYzXQtTkXAVgOG0MWgfRBFfrS45dF6tLptg1ZpmkuI7V1S0aSbrR1H0uFqmIpUfykNgJduC9szEW2xNyx1eB0YalgbWR7UQCVN2lnEFXuQKJkFu9OrKlgq/rXPBIYNLnbxdYhGv72FsF1Aba4/Yk7VlEU/CjJuttIMOBkAeh1uY5OrhJyN0lmdFkc0sZCzYJFChZg8nw02iGnBiCmcRlmS9EdakQ3qKTiI0iuVacDcPe1o4ELG2yqSi1h6BjlybtN8SRNLl+CI6mj0W7NK5qGDV0xoijqOZg0FrM8tw4Npnnh289ouYFMASbRzpPBnI9gn9ALTP8Qa0FJUMaptEnE5xtkxlxDqRHapeNx0PJkCICHFUNxUcDeumhd1EmMR4cByYPUQW6H10Lz2FqLUsmskRNMKXnEClIm2VonTrT9fVACZVcA5ea6jkksu8ZX4N8cQQh5hWyS4AGCiUXMA2CdYdzsCJdcZmsTnLUdd00kmfyoK5JzehWxWExqSuWfMOj3LkLYx04qcmeJ1sTSEdbAvM191orAXTvremNIROlP9bvHK/DJ+r6jPHvHE/B+WJiRYQjqjPGvTANX3Y+7ZZZ3vedvZJXrfTroGOkt62Dfa9Yhv3etkujfSwxzFmPz+Ufv26z620eqVGqTNz7h3j7hNH2VO4ZXkCi5GnzOs/7KfWHo27z5v5zXBjoc89XX/HtAm+B3Nei/Ya5pXbwg/eH4pW9Pu34C2MWQzWmqlvr982//xO5I+03YD3KsALRiIVxidbxTMYWiFEyUrbrVaKxOKYF8QWA849sKr7LLTnTc++Xq1mIFdYJM82H5993WzkZz+x4OM8eBmuVsHPnz/7usqJGUDQDWKLF9QV/ARJmK1TAdnrqrHo9Tryt4cl1qFrmNqKssADkvaqIUhBmhrm2SylQBnbWUVbbKj2D52qaXIKAxhkJ3xytzj+1++5M6GUVL7t+i4Lx1AIy8RJrau2itYKrDMsmHf6GtgYIymhkk41A4XVlKmN7JkmaGv2HOyko0RrqFXyUHnoWjew9j+rEYZtzqHD1LaVXR8x3KpSG5mdIxZ2TunHrc+mGI0s422o9wJW0qEBBaZ8sBTFwZVr49bRtI5dBDdqV47md/sOxxh1CK9H/GAlThWDFJjkspFCFAzUHQR8EFz73G2bmYEEthO5B87UCBVnwBlG+EPysKRJQrHMCZKxvHHxOBrMcB2Lh6XHkraok4LRSnSYisy6ykU3hKiISJpi745ezS2h4hv+hBFSa456/vWz4+TTTK7noG44VSgIcEa8sOM04qWXvVDbUfwdtXDVMCqqcwQU2UhbFNeGKsA5gWplN1xEi+p+AfOXxsJAQujBW2k2nSz2iNrCWxZ8gmyNbR13XVUAAXuwE3z/VVX9n4aptrpAG3D4dY5Whp+Pf/29MY+4mczus/0+nYMovlC4v1/NoSIPZ8+ntFSXkY6ViFw0+rs0Fifvr071UhqwlAx3YJW3E39odOUSFeQzSIfpVBq8MSBze7J1N++hsHDC/Yg+GBflYNEY1DnRT6Vh9NldiPZJUmXMjsJPMQyHWI8liboILuo/cTl78dOnuHYKMfZMf75YIdfOsKvLi0+xyFJffBrJ5BBGB1gOggRyg6ytMejb1lzh7v4xxe46gaPMzcrfsD2P3GIV22aXTTU1g2h1fZi6x1KxaZlVIC7w2/NVBVB7KscC6w2T9WmEH0EMdWe5rFbInXspFem0n/Li7ubohZtgfz+lidLbKRVjZL/ktOQn9kt6Rfj2FnrEsd2SXgZqs6RfPbGF0CMNbpVU7OGdkhbp7I2SVu5J+yTtRvn7SO6StLD+xkYL9u6RdFjkFsmRs1cw5ZM9qHeEGxcWKkQfMruYJaDGdh9h/mWBg5msDe8c0sfcbUI9PPHPS4JRLy37WzZNlsihAxBnaM9IbJIehjjaKFHMYpWuB0h1TDwTm1yqzjqoOsIGLTVXkz++e/vtN//eSsrWZR6T1gndOImuL1oH14KLj/vxTfx0VhlJsyUI6OKXRam3dD+RUsmCXgD0eV6Z2fbiCaofFlkhla2jO6wQPUaMXet8GO3Hw6AkH5qCfOjK8WFEjICDcCK0KbeJ522Cg6tjZFzY2BLrmcFbKs7V3jIRPLvEtYRgwd9OB08udYqBPIzbx8OAhg+leEC3a7azB7ZjVk/4e4BlQxFIwT1NjINhisdEKkflEmfoF4IIXOsj5L2dd91cm5zLrMx7WxEkFevHGSrQPBNTMoCHR2O1p3eZ+Mhqj01nxLExQTQN2lTZYJaJwonjCQpqkmwbsAEQHVHVW25dueO54DHlO47z4uW719+//e7b8M3b96+/f/1f37x//eaHd0OFuOjZuCiMcDBGJMfpsd7mjnTKrFMh/W8rRy1MWDOksnejcSzTWuYm2HOx2Xb3dj05U7nnKmaahxOrcsaE/C04WTzV0P3IwSMGEIIJv5xWT5VFFywG+XGQOl6YQsfO8ol9s92C4LDAcMxGYovhDGDBpp1hQi02k+YpBBaT9ZVxvP/VzDQiQAyumcLlN0RzIjm3vm72c/sY4wKtHjCv3r27ns1HDL92VqE3iurmwMAQVoRZMZHOswLPKWy4B/aOe7/1MMC1Vz/Savco5ax7W8nH3YqEB42JiY5PW1n2yu6T9QLq9okFUXlrA723zmieOdCgPX5yUekgw6mlSzkd3A9uaPbZbjezq7o4yacu2ZHqefoBCPl2BNLXKcRqYcbykqW9TWpXZB2QgWnFjzhpnN6oAO7WHHRYh7jI7c3QCYRaaJfPUbZfPgcdwMdGrOGzyDeYUOBnYw8X85stP+7f9kt36NSiYSKGT2MwXdrPvVzXv5bwG4rRzcpb+yT92t2xzxhMWMua7aAtOTaouUb5ZV+E1auxMBHq76urDaiNq8Pq6vkwr1LT6gqCAcOU7maobvdU7OOmJURKcnUFMltdgdjrU93u0W83dxE4uayu4PdQPaiIQTjWwziocLj4YjOI4+XT1sC0FmknyaNauiPiQ8lLz2HACa4PApa3zcEmVcwbmwLdknETyClqYCQ3SI02NC7/WMtiucxFzNKQXArZcvC2KcyjiWBhKiLFevtOzvkofSqjY79rU8y2sn0cUafXy5VuOu5dUem4lUOnm62Hv6P8GUCJzJjd4P/p5w7i7oZc2p3Lbia+ZmVq8I2bpntalFH3AVyNKLEjoGrrIVF4ChineC8wFMkgS/E1PiYIjXzk+SBz7W5Pj5RXsYwwaU8bFLFUaZeGocFeQJCK+8y4Cx9uXSTuYxWCbv6JgjYzw1LASSW9dnV4Tpu6q84Ty6NUfT0jdAKpxNtEB0Rl/DgAzOpLJMgodcDxUtCiQ42rfdlWW7FSbyPBrFIRh+6ipB0C9dhv8cL6TlDKrYjrcdctMV5T3YZUutsA0N0+AuPdBmrfDCGRlAW2hJYFIJNkEa+XdBeX3j4uyQYufS1c1k1s8pWGBlLeywJEg2x6W3g9GCdpsG5PoWTzLOIRoYIIza33bGiGJ0bdc4I+jXrZ2yMR74R7HFyuiJjX51GEL6H5fd3QvPGu1MzGecYl7kj6c3i8C/tYKk5q2yGEvBzgVWgFUxp1kEel7gKYZ06rQE9WiEF4ihsZM7vTQJrCiUMdKrUZHkM5EYb8daMxnuEvIVxNpLcdNU6d+TTxjzLnvdprkBZjjXpaxh4Z5PI2rIJ97apgiJtFGnYON9tm4Igsrv1FtdeAS/Q+DbuJJDD1pHRZ+iS6uxXo6S1PNjwGH19P993+HHFykj+idNVH2Cdq460XIbJKBDwdNd6KjovZ6p5UtblL2feJStdywkfrQqWFrRPJFr70R4l9jieMOfHsf4eLwvXcYHsswT6TPfbdwxyRzdI7ZVVXKnE1Qq22xi+gNljHy6c/97H2hlATaO8GNRBiYf5LtZA+LnUbrxW7a+o21b59hHmJGfTY+6F1Kyyg11GAkoLpJIeWrK7WMGFFUj4GuGWHG+iwKt8yAFO7ddZfxNYv2nX3tjyAaz2wwHRgvelz2QY820gIxaksE3s1tA4A+mOpw3MmPM5LBJ5ZRaUdoT2DRvKEqa5N8zjzNome8JDTcB8TOOSwb1H8I5+mkYO/Q/WM/y6LcgEUx+sFOmTSEXQLJH0BkuwLuoIp6KSdmQekQlO9kunScTmVHzpF75jKhd5O3XOt6fRgqlFq0NRY48ZMLa4Iv7EAj5TOeBPXykM+jGsx6C2oFiWXN/ObfXc/mqLg439+usxy8mpKRtLo1WpmnloPIlvXcqstxVPazCbD78+esvTXJ53+ig7uGf10pC0rrR/0zB6RZfVbHb8MHLt54DHGhSSTds+NCR5/KoTq3luiWtp4u7e4xR28uZ+cy9PNzZEu1YZjVPdGl5ddSFh3Wydm/w6rhXjGnk5fA0B21bDNaBX4/WbjUqu+cmScaB+c4YTQu8XXItdXgyao11LHtWtpU83L3ac6WsJIoeQrTqI7E1XbIJ+vWdfyYcVazpheqwtjo2qVZsvVZME69jTFOu64Zh1vfNxWLZ02bh15yritujRRuU32+dqtujqsXkca069jTVbwxHFbldeZVUiHD3M9BO/mMKu+Z6l62BDilo/M01ZQNimjTefTs+EXZ2FF4UnHdM6t7l3IoktGIsS7CmJ3sXMvy/26POZ6WstJpQOvXbjGEHg0C0tTuR+XylPvooKX126FvdI2UnZspnWxeByMWZq8WSHy8V5VxBk+5i2LaXz+BNHmVOVhBjB6PHLsbJmO8msjnw/nsI8JFH7XztFdWAGdlautslwGNR7stzwPErnPNwpC0+FCYz1NPcCrOumW5yMyOdGtmUxgR0xPGOjIXKvOOtFPrZbrE8uV+UQmPscppnLxy7amUd1Xkk0g2kPiaVR3dDONe/xKsUnCUtkZ6pqxPOa6ut41RWosTyL5NFHLW9H+srcBybEcggV9lp55yid21fGZMeoM2zijeKRPL30v1SOfyK1O2yeR1TTHb5ln+oxjholeA/iT/QZwp3sOIJ/jO7Dsqd4DuGf4D2RP9iBAnupDgDrdiwD5DD8C7DM8iRXdRF9Sq/BMb4IyPMOfoO4nexSU4zk+5aj9ycP+mGPyyK9t5qwqzvItkOEM7wLsyf5lPS0cWRezHUvL9uGCe3Q8+UmlY3ueVDpQUY+xDWYwxBtge7ex0cUjMPw4GB+uqfbgOmL2fanncTBbBNdLFs07eRy2XPjSF1R6hIvmYEtCL/xQrnbLOVlRlJvdDQ0lTO1FTiEbE+RQ260HK64XfuzuBd1jxO5vaWybPVzfk0AROC3T4GqldXo6BvageDxEMsy9vqYAkcAIZFHIFos52ayKgMp9yTwEqbF+OnuW3d8lJFIwMv1xPl+T6euHvaABScrscW3odIHfIUUhaTC/i0lgE6zB6EjRVlg6gGkKy27u55EmReogsomwmNzzlKws44v76ycagVnXI+5MJPHNS1J3mRagV54/yAP8XszhAyZZujMf7+bkIAdgcUcBOX+ii8rBhO2rHQosNvOXN/TAKjY3C49jKLLi9vZ+TqrWYi9e0JjEy2++MfUh292T6tMb8BqkNAAxiyWJbHcBX17TJqa3+yC7nt/u/eCLOQ1m4KVoB6YzBjEvi0hM5gfsxZLuRbENilta1LpUa4+lQV3z+8CjIgAXcwDJCgG8ubvlJSkcBO9vPIjHnRq+USyXtDc16/v5nM62Ffmjp3f4pSU+SKaGwXQ75Qs/im11den/dp7/DGJXJUXyaxStVsnnv2acb+Sz2ef+b2VZ3syfrpekbpczcB0XWVZQ4GoVFUXc/uL2UxNj/DL5C/A9dGSR4pcDkM6QZZKcLPALmaQmBwTTtENiZZAJ0j+wMhFyJ0mfy3YsNxtJQRFLSL8R8fwDmS4UaVdRyuLHiCt18KAf6a9viZOMTIZ5xZofCZZFzB5JKGH5hq4oSYRzoSSI3w1OyschoPetCebXpKgSWUhSJonijOwdT/FK5urq9OiuDSt8YCvJEJGXiVRsz0kTXaek9NdKHsjOuRv3kTSBfyxtmaAFuoUlmSSH0baYQUBFms/WkMYLyShirshmOjRjG9ryAQ69okbQn7MESZJDUwxIRPBxbHVFR6AnmFb9ifCShPNYbnJhSDEJ74ARHgN9CLxZHmG8kOkyfyRHweOeFPHjQYLfZKur/ROsXuiglefguOiQlU5dXZUvX5LygyB3s7+jXX+64WBGO9JDpuJDKRKAoxJ3Pshgl9MeAoJWchRgerCk1ZxlfE030jMzOavwaivDIRx5whkHXvvBnQeQSqb0mkALFOViTvcu58bek6OxPUz1HsSTA4JzqI0cUDXokUuOL9LyBP13Qgf4J8JekAYPo432Srl8FKR0wJqZUiQkC557fDc+32bo5kROtuOIgyV48ZkPLRZ385IEWEr6zYLlTOOTPQ9oeExuiRTJ04aM/gtyLBdbkYqCVI3X2IuUHXzrIMTwS8iEzEFW9rsFItpTWyzNMrr3Sj7BDE3aY6HJEfrBcHq9hNtHJAAxL1m5Zpkuc1JcFRQMFJrTU73+n/aupkdtGIje+ysqjpXILkvZbc+r7bk/ACkywQQvJKax+arEf69n7IR8jKO9tL3MO4HehBh74sTOzBt6uAyUUPkW+S1JDr6B64UmQOX2+TH54lx8p8jOq22WDwt67jA53c6tqMhuR2Jqf03zC70Ud0tLeosC15xRJzN6Y1eipIfUrUjHntHavIy0qmVypO8EbZPYfzvQnXUtVkrQTfMUzJjkRRd4mqwfE2EWuqLupfs0Jx21ZardFEOaCGs16UhWm61akRMonpUkKkn/lnmaqSndfcdDEnbXIyyonpGUJfvnJCtLTrInvRabiKOdBenX7hIqIjusjqLH56z2+0wX9EHKXbVQeOxzNsYnUTbSzgu9fLxoTc5tv2185wE/1SqdTbDHB99mNPb0+4yGJt9oGAlL8dRnGfybDY+6QZE9D78yWy4hrpDqx6vYah2nV6rM46wwuzhZmDKQHxonEFHRJQSzZ7v0aLwGdztZPfBnKe0wuwPHNsm2Mtv1D0Omn2x/VuscMoBBhtEmEKkN/Ten2NQ9T5rGZNE1QYHnA8hI0+4yMAsZJIN8r4EhxCimzdd+JobFVI5C2ZB+B/fSSbs8zd0gBOQ35V7mj0MbH2OJkikTkKKNWaA8KUmvy7QRaYwZ+HqNsSY4g9EmOt7rV4ycwfq3x9TxPnsjrWoN3NnT0MaLezgbr+izbmTRWhoU3g0VlgvNQoLJ88vXxctsMf/eNUJVjNQXvu56pby4xYJBOSBRR561ppFNJXJQFMJOh2HvVqNbPrzVx6c/gukrjs9PDBe4lxK4xc6X4rrKO03z2ndota70oZvy4gMS3A0tc6ay75aB7UtkvGOEbKFyN/nKWnuuf6n4Eno+z7nbHd2Q6MleG9Mp7hf0ensp9UidlLkXG3GLvFMt7nwjzjp2i4DB+3T734W2GQwGg8FgMBgMBoPBYDAYDAaDwWAwGAwGg8H4i/gDlESXpgCgAAA="|base64 -d |tar xvz
        sed 's/127.0.0.1:11211/memcached:11211/g' -i /var/www/html/wp-content/w3tc-config/master.php
        )


    )
    test -e /var/www/html/wp-content/w3tc-config/ && wp plugin activate w3-total-cache

    test -e /var/www/html/wp-content/object-cache.php && rm -rf /var/www/html/wp-content/object-cache.php 
    # git clone https://github.com/Automattic/wp-memcached.git /tmp/wp-memcached && (
    #    echo "stats"|nc memcached:11211|grep STAT -q && (
    #            test -e /tmp/wp-memcached/object-cache.php && (
    #                    ( echo "<?php" ; echo '$memcached_servers = array("default" => array("memcached:11211"));' ;tail -n+2 /tmp/wp-memcached/object-cache.php ) > /var/www/html/wp-content/object-cache.php 
    #                    )
    #            ) 
    #             
    # )
    #echo "ffpc";
    #wp plugin install wp-ffpc --activate  --path=/var/www/html;
    #FFPCVER=$(wp plugin list --format=csv|grep wp-ffpc|cut -d, -f4);
    #cacheme=no;
    #grep "^define ( .WP_CACHE., true )" /var/www/html/wp-config.php || ( echo  enabling cache ;sed 's/<?php/<?php\ndefine ( '"'"'WP_CACHE'"'"', true );/g' -i /var/www/html/wp-config.php
    #mysql -u ${WORDPRESS_DB_USER} -p${WORDPRESS_DB_PASSWORD} -h ${WORDPRESS_DB_HOST} -e "select * from wp_options WHERE option_name LIKE 'wp-ffpc%' " ${WORDPRESS_DB_NAME} | grep $FFPCVER && cacheme=yes
    #echo "$cacheme" |grep ^yes$ && (
    #                             echo ffpc settings config in db .. VER $FFPCVER ;
    #                           )
    #echo "$cacheme" |grep ^no$ && (
    #verlength=$(echo -n "$FFPCVER"|wc -c)
    #echo "installing settings for ffpc ver $FFPCVER ( l: $verlength )";
    #cat /ffpc-template.sql|sed 's/APPURL/'$APP_URL'/g;s/VERSIONLENGTH/'$verlength'/g;s/VERSIONNAME/'$FFPCVER'/g' |  mysql -u ${WORDPRESS_DB_USER} -p${WORDPRESS_DB_PASSWORD} -h ${WORDPRESS_DB_HOST} ${WORDPRESS_DB_NAME}
   #)
   #)
)

