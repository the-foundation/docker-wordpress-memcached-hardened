saveresolv=$(cat /etc/resolv.conf)
(
        echo "nameserver 1.1.1.1" 
        echo "nameserver 8.8.8.8" 
)> /etc/resolv.conf
which curl &>/dev/null || apk add curl 
_buildx_arch()           { case "$(uname -m)" in aarch64) echo linux/arm64;; x86_64) echo linux/amd64 ;; armv7l|armv7*) echo linux/arm/v7;; armv6l|armv6*) echo linux/arm/v6;;  esac ; } ;
export OSVER=alpine
export DROPBEAR_VERSION=$(curl -s "https://github.com/TheFoundation/hardened-dropbear/releases"|grep expanded_assets|grep 'src="https://github.com/'|sed 's/.\+expanded_assets\/v//g;s/".\+//g'|head -n1);
export HDB_DOWNLOAD_URL="https://github.com/TheFoundation/hardened-dropbear/releases/download/v$DROPBEAR_VERSION/hardened-dropbear-$OSVER."$(_buildx_arch |sed 's~/~_~g')".tar.gz"

( 
    cd /
    echo "trying binary install with >curl $HDB_DOWNLOAD_URL"
    curl -L "${HDB_DOWNLOAD_URL}" | tar xvz
)

which dropbear &>/dev/null  || (
apk add gcc make autoconf ca-certificates git zlib-dev libc-dev && beartarget=/tmp/dropbear &&  cd /tmp/ &&  git clone https://github.com/mkj/dropbear.git $beartarget && cd $beartarget && echo varsetup && echo '#define DROPBEAR_AES128 0
#define DROPBEAR_DEFAULT_RSA_SIZE 4096
#define DROPBEAR_DSS 0
#define DROPBEAR_AES256 1
#define DROPBEAR_TWOFISH256 0
#define DROPBEAR_TWOFISH128 0
#define DROPBEAR_CHACHA20POLY1305 1
#define DROPBEAR_ENABLE_CTR_MODE 1
#define DROPBEAR_ENABLE_CBC_MODE 0
#define DROPBEAR_ENABLE_GCM_MODE 1
#define DROPBEAR_MD5_HMAC 0
#define DROPBEAR_SHA1_HMAC 0
#define DROPBEAR_SHA2_256_HMAC 1
#define DROPBEAR_SHA1_96_HMAC 0
#define DROPBEAR_ECDSA 1
#define DROPBEAR_DH_GROUP14_SHA1 0
#define DROPBEAR_DH_GROUP14_SHA256 1
#define DROPBEAR_DH_GROUP16 0
#define DROPBEAR_CURVE25519 1
#define DROPBEAR_ECDH 0
#define DROPBEAR_DH_GROUP1 0
#define DROPBEAR_DH_GROUP1_CLIENTONLY 0
' > $beartarget/configvars &&  echo -n replacing" " && for var in $(cut -d" " -f2 $beartarget/configvars);do echo $var;sed 's/.\+'$var'.\+//g' default_options.h -i ;done && cat /$beartarget/configvars >>  default_options.h && autoconf  &&  autoheader  && ./configure |sed 's/$/ → /g'|tr -d '\n'  &&    make PROGRAMS="dropbear dbclient dropbearkey dropbearconvert " -j$(nproc)  &&  make PROGRAMS="dropbear dbclient dropbearkey dropbearconvert " install || exit 222
        ln -s  /usr/local/sbin/dropbear /usr/local/bin/dbclient         /usr/local/bin/dropbearconvert  /usr/local/bin/dropbearkey /usr/sbin   ;     rm -rf $beartarget 2>/dev/null || true
apk del gcc make autoconf git zlib-dev libc-dev
)

which drobear &>/dev/null || apk add dropbear

test -f /usr/libexec/sftp-server || (mkdir -p /usr/libexec/ && ln -s /usr/lib/ssh/sftp-server /usr/libexec/sftp-server)
echo "su -s /bin/bash www-data" > /usr/bin/wwwsh;chmod +x /usr/bin/wwwsh
test -e /root||mkdir /root
grep wwwsh /root/.bashrc || echo 'alias wwwsh="su -s /bin/bash www-data"' >> /root/.bashrc

[[ -z "$saveresolv" ]] && { echo "$savereseolv" > /etc/resolv.conf  ; } ;
